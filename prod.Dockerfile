FROM node:alpine AS builder

WORKDIR /app
COPY . /app/
RUN npm install -g @angular/cli
RUN npm install
RUN ng build --configuration production --optimization


FROM nginx:alpine

COPY nginx/default.conf /etc/nginx/conf.d/
## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /app/dist/adamtahri/* /usr/share/nginx/html/
CMD ["nginx", "-g", "daemon off;"]
