import { Component, ViewChild, ElementRef, AfterViewChecked, Injectable } from '@angular/core';
import { OutputLine } from './output-line.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-terminal',
  templateUrl: './terminal.component.html',
  styleUrls: ['./terminal.component.css']
})

@Injectable()
export class TerminalComponent implements AfterViewChecked {
  @ViewChild('cmd', {static: true}) cmdElement: ElementRef;
  @ViewChild('terminal', {static: true}) terminalElement: ElementRef;
  lines: OutputLine[] = [];
  history: string[] = [];
  historyIndex: number = 0;
  prompt: string = 'guest@ubuntu$ ';
  cmds = {
    ls: this.ls.bind(this),
    uname: this.uname.bind(this),
    help: this.help.bind(this),
    echo: this.echo.bind(this),
    cat: this.cat.bind(this),
    clear: this.clear.bind(this),
    date: this.date.bind(this),
    reboot: this.reboot.bind(this),
    linkedin: this.linkedin.bind(this),
    gitlab: this.gitlab.bind(this),
    wget: this.wget.bind(this),
  };

  files = [
    'cv.pdf',
    'README.md',
  ];

  constructor(private http: HttpClient) {}

  ls() {
    this.print(this.files.join('\t'));
  }

  uname() {
    this.print(navigator.appVersion);
  }

  help() {
    const help = {
      ls: 'list directory contents',
      uname: 'print system information',
      help: 'list informations about available commands',
      echo: 'display a line of text',
      clear: 'clear all or part of a curses window',
      date: 'print the system date and time',
      reboot: 'reboot or stop the system',
      linkedin: 'open linkedin profile',
      gitlab: 'open gitlab repository of this project',
      cat: 'display content of a file',
      wget: 'download and open a file',
    };
    Object.keys(this.cmds).forEach(cmd => help[cmd] ? this.print(cmd + ' - ' + help[cmd]) : this.print(cmd));
  }

  echo(args) {
    args.shift();
    this.print(args.join(' '));
  }

  cat(args) {
    let filename = args[1];

    if (filename === undefined) {
      this.print('USAGE: cat FILE');
      return;
    }

    if (this.files.includes(filename) === false) {
      this.print(filename + ': file not found.');
      return;
    }

    this.http.get('/assets/' + filename, {responseType: 'text'}).subscribe(data => data.split('\n').forEach(line => this.print(line)));
  }

  clear() {
    this.lines.length = 0;
    this.history.length = 0;
    this.historyIndex = 0;
    this.cmdElement.nativeElement.value = '';
  }

  date() {
    this.print(new Date().toString());
  }

  reboot() {
    this.print('rebooting...');
    window.location.reload();
  }

  linkedin() {
    window.open('https://www.linkedin.com/in/adam-tahri', '_blank');
  }

  gitlab() {
    window.open('https://gitlab.com/gotakk/adamtahri', '_blank');
  }

  wget(args) {
    let filename = args[1];

    if (filename === undefined) {
      this.print('USAGE: wget FILE');
      return;
    }

    if (this.files.includes(filename) === false) {
      this.print(filename + ': file not found.');
      return;
    }

    this.print('Downloading and opening file ' + filename);
    window.open('/assets/' + filename);
  }

  onPressEnter(cmdLine: string) {
    this.lines.push({prompt: true, content: cmdLine});
    this.cmdElement.nativeElement.value = '';
    this.history.push(cmdLine);
    this.historyIndex = this.history.length;

    if (cmdLine !== '') {
      const args = cmdLine.split(' ');
      args[0] in this.cmds ? this.cmds[args[0]](args) : this.print(args[0] + ': command not found');
    }
  }

  onPressArrowDown() {
    if (this.historyIndex < this.history.length) {
      this.historyIndex++;
      this.cmdElement.nativeElement.value = this.historyIndex === this.history.length ? '' : this.history[this.historyIndex];
    }
  }

  onPressArrowUp() {
    if (this.historyIndex !== 0) {
      this.historyIndex--;
      this.cmdElement.nativeElement.value = this.history[this.historyIndex];
    }
  }

  // TO REWORK
  onPressTab(event: Event, cmdLine: string) {
    event.preventDefault();

    let args = cmdLine.split(' ');

    if (args.length == 1) {
      let cmds = Object.keys(this.cmds);
      let regexp = new RegExp('^' + cmdLine, 'g');
      let filteredCmds = cmds.filter(el => regexp.test(el));

      if (filteredCmds.length == 1) {
        this.cmdElement.nativeElement.value = filteredCmds[0] + ' ';
      } else if (filteredCmds.length > 1) {
        this.lines.push({prompt: true, content: cmdLine});
        this.print(filteredCmds.join('\t'));
      } else {
        this.print(cmds.join('\t'))
      }
    } else if (args.length == 2) {
      switch (args[0]) {
        case 'ls':
        case 'cat':
        case 'wget':
          let files = this.files;
          let regexp = new RegExp('^' + args[1]);
          let filteredFiles = files.filter(el => regexp.test(el));
          console.log(filteredFiles);
    
          if (filteredFiles.length == 1) {
            this.cmdElement.nativeElement.value = args[0] + ' ' + filteredFiles[0] + ' ';
          } else if (filteredFiles.length > 1) {
            if (files.filter(el => el === args[1]).length) {
              this.cmdElement.nativeElement.value = args[0] + ' ' + args[1] + ' ';
            } else {
              this.lines.push({prompt: true, content: cmdLine});
              this.print(filteredFiles.join('\t'));
            }
          } else {
            this.print(files.join('\t'))
          }
      }
    }
  }

  onPressCtrl(event: Event) {
    if (event['key'] == 'l' && event['ctrlKey'] == true) {
      event.preventDefault();
      this.clear();
    }
  }

  print(str: string) {
    this.lines.push({prompt: false, content: str});
  }

  ngAfterViewChecked() {
    this.giveFocus();
  }

  giveFocus() {
    this.cmdElement.nativeElement.focus();
    this.terminalElement.nativeElement.scrollTop = this.terminalElement.nativeElement.scrollHeight;
  }
}
