FROM node:alpine

RUN mkdir -p /usr/app
WORKDIR /usr/app
COPY . .
RUN npm install -g @angular/cli
RUN npm install
EXPOSE 4200
EXPOSE 49153
CMD ng serve --host 0.0.0.0
